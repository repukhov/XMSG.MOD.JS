/*
   У элемента должен быть class="help". Напр.: <span class="help">?</span>
   В функцию передаётся this и сообщение, которое нужно показать 
   Вызов: Help.Show(this,'#message#');
*/

;(function(){
    var object = {
      Help : function(msg){
        var event = event || window.event;
        var elem  = event.currentTarget;
        var pos = this.GetCoord();
        posx = pos.px;
        posy = pos.py;
        //posx and  posy
        //**************  posx and  posy ***************
        if(elem.dataset.helpFor === undefined){ 
          var div = document.createElement('div');
          div.style.position    = 'absolute';
          div.style.background  = '#72b525';
          div.style.color       = '#fff';                    
          div.style.border      = '2px solid orange';
          div.style.width       = '250px';
          div.innerHTML         = msg;
          div.style.top         = posy + 'px';
          div.style.left        = posx + 'px';
          div.style.marginTop   = '-20px';
          div.style.marginLeft  = '20px';
          div.style.borderRadius = '7px 0 7px 0';
          div.style.padding     = '5px';
          div.style.boxShadow   = '10px 10px 10px rgba(0,0,0,0.5)';
          div.style.boxSizing   = 'padding-box';
          div.id                = 'help_' + Math.floor(Math.random() * (1000 - 50 + 1)) + 50;
          //div.dataset.helpFor   = div.id;          
          div.addEventListener('click', function(){ 
              document.body.removeChild(document.getElementById(div.id)); 
              elem.removeAttribute('data-help-for');
          });
          div.onselectstart = function(){ return false; };
          div.addEventListener('mouseover', function(e){
              var elem = event.currentTarget || window.event.currentTarget;
              elem.style.cursor = 'pointer';
          });
          elem.dataset.helpFor = div.id;
          window.onresize = function(e){
                var elems_div   = document.querySelectorAll('[id^="help_"]');
                var elems_span  = document.querySelectorAll('[data-help-for^="help_"]');
                for(var i = 0; i < elems_div.length; i++){    
                      var pos = elems_span[i].getBoundingClientRect();
                      var help = document.getElementById(elems_span[i].dataset.helpFor);
                      help.style.top      = pos.top + 'px';
                      help.style.left     = pos.left + 'px';
                      help.style.marginTop   = '-20px';
                      help.style.marginLeft  = '20px';
                } 
          }         
          document.body.appendChild(div);
        }
      },
      Error : function(name, msg){
          var elem = document.getElementsByName(name)[0];
          if(elem.dataset.action === undefined)
          { 
            elem.dataset.action = 'yes'; 
            var pos  = elem.getBoundingClientRect();
            var div = document.createElement('div');
            div.style.position    = 'absolute';
            div.style.background  = 'yellow';
            div.style.paddingLeft = '5px';
            div.style.margin      = '0px';
            div.style.width       = elem.clientWidth - 5 + 'px';
            div.style.height       = elem.clientHeight - 2 + 'px';
            div.innerHTML         = msg;
            div.style.top         = pos.top + pageYOffset + 2 + 'px';
            div.style.left        = pos.left + pageXOffset + 'px';
            div.style.boxSizing   = 'padding-box';
            div.id                = 'error_' + Math.floor(Math.random() * (1000 - 50 + 1)) + 50;
            div.dataset.errorFor      = elem.name;
            div.addEventListener('click', function(){ 
                document.body.removeChild(document.getElementById(div.id));
                elem.style.boxShadow = '';
                elem.removeAttribute('data-action');
                elem.focus();
            });
            div.addEventListener('mouseover', function(e){
                var elem = event.currentTarget || window.event.currentTarget;
                elem.style.cursor = 'pointer';
            });
            div.onselectstart = function(){ return false; }
            window.onresize = function(e){
                var elems = document.querySelectorAll('[id^="error_"]');
                for(var i = 0; i < elems.length; i++){    
                    var elem = document.getElementsByName(elems[i].dataset.errorFor)[0];
                    var pos = elem.getBoundingClientRect();
                    elems[i].style.width    =  elem.clientWidth - 5 + 'px';
                    elems[i].style.height   = elem.clientHeight - 2 + 'px';
                    elems[i].style.top      = pos.top + pageYOffset + 2 + 'px';
                    elems[i].style.left     = pos.left + pageXOffset + 'px';
                }  
            }
            document.body.appendChild(div);
            elem.style.boxShadow = '0 0 5px 1px red';
          }          
      },
      ErrorAbort : function(name){
        var error_div = document.querySelectorAll('[data-error-for="' + name + '"]');
        if(error_div.length > 0)
        {  
          document.body.removeChild(error_div[0]);
          var elem = document.getElementsByName(name)[0]; 
          elem.style.boxShadow = '';
          elem.removeAttribute('data-action');
        }  
      },
      GetCoord: function(){
          var posx = 0;
          var posy = 0;
          if(!e) var e = window.event;
          if(e.pageX || e.pageY){
            posx = e.pageX;
            posy = e.pageY;
          }
          else if (e.clientX || e.clientY){
            posx = e.clientX + document.body.scrollLeft
              + document.documentElement.scrollLeft;
            posy = e.clientY + document.body.scrollTop
              + document.documentElement.scrollTop;
          }
          return { px:posx, py:posy };
      }      
  }
  window.XMSG = object;
})();

